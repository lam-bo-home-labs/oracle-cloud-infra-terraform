Terraform plans for a free kubernetes cluster on Oracle Cloud Infrastructure 

References 
==========

- https://arnoldgalovics.com/oracle-cloud-kubernetes-terraform/
- https://medium.com/oracledevs/hardening-the-oci-terraform-provider-authentication-security-c0f2b92ffb3a
- https://docs.oracle.com/en-us/iaas/Content/API/SDKDocs/clitoken.htm#Tokenbased_Authentication_for_the_CLI
- https://docs.oracle.com/en-us/iaas/Content/API/SDKDocs/terraformproviderconfiguration.htm#securityTokenAuth


Prerequisites
=============

- Oracle account upgraded to paid
- Oracle CLI (i authenticate with security token)
- Terraform
- kubectl

Installation of OCI infra terraform stack
=========================================

Create and fill a variables.sh file based on the variabls.sh.template file inside oci-infra

```
cd oci-infra
cp variables.sh.template variables.sh
chmod +x variables.sh
./variables.sh
```

Apply the terraform plan

```
oci session authenticate
terraform apply
```

Note the terraform outputs for the next part

Installation of K8S infra terraform stack
=========================================

Create and fill a variables.sh file based on the variabls.sh.template file inside k8s-infra

```
cd k8s-infra
cp variables.sh.template variables.sh
chmod +x variables.sh
./variables.sh
```

Apply the terraform plan

```
oci session authenticate
terraform apply
```

Access to kubernetes cluster
============================

```
oci ce cluster create-kubeconfig --cluster-id <kubernetes-cluster-id> --file ~/.kube/home-labs-config --region eu-paris-1 --token-version 2.0.0 --kube-endpoint PUBLIC_ENDPOINT
export KUBECONFIG=~/.kube/home-labs-config
kubectl get nodes
```

Notes on updating kubernetes version on node pool
=================================================

Since we use Oracle basic kubernetes cluster, it's not possible to rotate nodes to update kubernetes version on them, so for update set the numbers of nodes to 0 in the pool node on the Oracle 
dashboards before applying the new terraform plan
