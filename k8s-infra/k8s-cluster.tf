provider "kubernetes" {
  config_path = "~/.kube/home-labs-config"
}

provider "oci" {
  auth = "SecurityToken"
  config_file_profile = var.config_file_profile
  region = var.region
}

data "oci_containerengine_node_pool" "home_labs_np" {
  node_pool_id = var.node_pool_id
}

locals {
  active_nodes = [for node in data.oci_containerengine_node_pool.home_labs_np.nodes : node if node.state == "ACTIVE"]
}

resource "oci_network_load_balancer_network_load_balancer" "home_labs_nlb" {
  compartment_id = var.compartment_id
  display_name   = "home-labs-nlb"
  subnet_id      = var.public_subnet_id

  is_private                     = false
  is_preserve_source_destination = false
}

resource "oci_network_load_balancer_backend_set" "home_labs_nlb_backend_set_pihole" {
  health_checker {
    protocol = "TCP"
    port     = 10256
  }
  name                     = "home-labs-nlb-backend-set-pihole"
  network_load_balancer_id = oci_network_load_balancer_network_load_balancer.home_labs_nlb.id
  policy                   = "FIVE_TUPLE"

  is_preserve_source = false
}

resource "oci_network_load_balancer_backend" "home_labs_nlb_backend_pihole" {
  count                    = length(local.active_nodes)
  backend_set_name         = oci_network_load_balancer_backend_set.home_labs_nlb_backend_set_pihole.name
  network_load_balancer_id = oci_network_load_balancer_network_load_balancer.home_labs_nlb.id
  port                     = 31600
  target_id                = local.active_nodes[count.index].id
}

resource "oci_network_load_balancer_listener" "home_labs_nlb_listener_pihole" {
  default_backend_set_name = oci_network_load_balancer_backend_set.home_labs_nlb_backend_set_pihole.name
  name                     = "home-labs-nlb-listener-pihole"
  network_load_balancer_id = oci_network_load_balancer_network_load_balancer.home_labs_nlb.id
  port                     = "80"
  protocol                 = "TCP"
}



resource "oci_network_load_balancer_backend_set" "home_labs_nlb_backend_set_yuzu" {
  health_checker {
    protocol = "TCP"
    port     = 10256
  }
  name                     = "home-labs-nlb-backend-set-yuzu"
  network_load_balancer_id = oci_network_load_balancer_network_load_balancer.home_labs_nlb.id
  policy                   = "FIVE_TUPLE"

  is_preserve_source = false
}

resource "oci_network_load_balancer_backend" "home_labs_nlb_backend_yuzu" {
  count                    = length(local.active_nodes)
  backend_set_name         = oci_network_load_balancer_backend_set.home_labs_nlb_backend_set_yuzu.name
  network_load_balancer_id = oci_network_load_balancer_network_load_balancer.home_labs_nlb.id
  port                     = 31700
  target_id                = local.active_nodes[count.index].id
}

resource "oci_network_load_balancer_listener" "home_labs_nlb_listener_yuzu" {
  default_backend_set_name = oci_network_load_balancer_backend_set.home_labs_nlb_backend_set_yuzu.name
  name                     = "home-labs-nlb-listener-yuzu"
  network_load_balancer_id = oci_network_load_balancer_network_load_balancer.home_labs_nlb.id
  port                     = "31700"
  protocol                 = "UDP"
}