variable "region" {
  type        = string
  default     = "eu-paris-1"   
  description = "The region to provision the resources in"
}

variable "config_file_profile" {
  type        = string
  description = "The name of the profile to be used for security token auth"
}

variable "compartment_id" {
  type        = string
  description = "The compartment to create the resources in"
}

variable "ssh_public_key" {
  type        = string
  description = "The SSH public key to use for connecting to the worker nodes"
}